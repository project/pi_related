<?php

/**
 * Administrative overview for related issue settings
 */
function pi_related_settings() {
  $header = array();
  $rows = array();

  $output = l(t('Add relationship type'), 'admin/settings/pi_related/add');

  $relationships = pi_related_relationships();
  foreach ($relationships as $id => $relationship) {
    $row = array();
    $row[] = check_plain($relationship->name);
    $row[] = l(t('edit'), 'admin/settings/pi_related/'. $id . '/edit');
    $row[] = l(t('delete'), 'admin/settings/pi_related/'. $id . '/delete');
    $rows[] = $row;
  }

  $output .= theme('table', $header, $rows);

  return $output;
}

/**
 * Return the form for editing a single relation.
 */
function pi_related_relationship_edit(&$form_state, $relationship) {
  $form = array();
  ctools_include('dependent');

  $form['relationship_id'] = array('#type' => 'value', '#value' => $relationship->id);

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Relationship name'),
    '#default_value' => $relationship->name
  );

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $relationship->label_0,
    '#description' => t('This is the label used when displaying this type of relation. You may use <em>!issue</em> as a placeholder for the name of the related issue.')
  );

  $form['directional'] = array(
    '#type' => 'checkbox',
    '#title' => t('This relationship type is directional'),
    '#default_value' => $relationship->directional,
  );

  $form['reverse_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Reverse label'),
    '#default_value' => $relationship->label_1,
    '#description' => t('This is the label used when displaying the reverse direction of this type of relation. You may use <em>!issue</em> as a placeholder for the name of the related issue.'),
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-directional' => array(1)),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validation handler for relationship type form.
 */
function pi_related_relationship_edit_validate($form, &$form_state) {
  if (empty($form_state['values']['name'])) {
    form_set_error('name', t('Name cannot be empty.'));
  }

  if (empty($form_state['values']['label'])) {
    form_set_error('label', t('Label cannot be empty'));
  }

  if ($form_state['values']['directional']) {
    if (empty($form_state['values']['reverse_label'])) {
      form_set_error('reverse_label', t('Reverse label cannot be empty.'));
    }
  }
  else {
    form_set_value($form['reverse_label'], $form_state['values']['label'], $form_state);
  }
}

/**
 * Submit handler for relationship type edit form.
 */
function pi_related_relationship_edit_submit($form, &$form_state) {
  $relationship = array();
  if ($form_state['values']['relationship_id'] == NULL) {
    $relationship['is_new'] = TRUE;
  }
  else {
    $relationship['id'] = $form_state['values']['relationship_id'];
  }

  $relationship['name'] = $form_state['values']['name'];
  $relationship['label_0'] = $form_state['values']['label'];
  $relationship['label_1'] = $form_state['values']['reverse_label'];
  $relationship['directional'] = $form_state['values']['directional'];

  $relationship = (object)$relationship;

  pi_related_relationship_save($relationship);
}

/**
 * Form for deleting a relationship type.
 */
function pi_related_relationship_delete_form(&$form_state, $relationship) {
  $form['relationship_id'] = array(
    '#type' => 'hidden',
    '#value' => $relationship->id,
  );
  $count = db_result(db_query("SELECT count(DISTINCT rid) FROM pi_related_endpoints WHERE relationship_id = %d", $relationship->id));
  $link = l($relationship->name, 'admin/settings/pi_related/'. $relationship->id .'/edit');
  return confirm_form($form,
    t('Are you sure you want to delete the relationship type !link', array('!link' => $link)),
    'admin/settings/pi_related',
    format_plural($count, 'All relations of this type will be deleted. There are is currently one relation of this type. This cannot be undone.', 'All relations of this type will be deleted. There are currently @count relations of this type. This cannot be undone.'),
    t('Delete'), t('Cancel')
  );
}

/**
 * Submit handler for relationship type deletion form.
 */
function pi_related_relationship_delete_form_submit($form, &$form_state) {
  pi_related_relationship_delete($form_state['values']['relationship_id']);
  drupal_set_message(t('The relationship type has been deleted.'));
  $form_state['redirect'] = 'admin/settings/pi_related';
}
