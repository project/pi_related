// $Id$

Drupal.behaviors.pi_relatedAddForm = function (context) {
  // Add related issue link
  $('#pi-related-add-form-toggle').click( function() {
    $('.pi-related-add-form-contents').show();
    return false;
  });

  // Cancel link
  $('#pi-related-add-form-cancel').click( function() {
    $('.pi-related-add-form-contents').hide();
    return false;
  });

}

Drupal.CTools.AJAX.commands.pi_related_update = function(data) {
  $(data.update_selector).before('<div id="pi-related-status-message" class="status">' + data.update + '</div>');
  $(data.content_selector).append(data.content);
  $(data.update_selector).fadeOut('slow');

  window.setTimeout(function () {
    $('#pi-related-status-message').fadeOut('slow', function () {
      $('.pi-related-add-form-contents :input[name="target_nid"]', $form).val('');
      $(this).remove()
    });
  }, 3000);
  
  Drupal.attachBehaviors($(data.content_selector));
  Drupal.attachBehaviors($(data.update_selector));
};

Drupal.CTools.AJAX.commands.pi_related_error = function(data) {
  var errors = data.data
  for ( var i=0, len=errors.length; i<len; i += 1 ){
    $(data.selector).after('<div id="pi-related-error-message" class="error"></div>');
    $('#pi-related-error-message').prepend('<p>' + errors[i].message + '</p>')
  }

  window.setTimeout(function () {
    $('#pi-related-error-message').fadeOut('slow', function () {
      $(this).remove()
    });
  }, 5000);

  Drupal.attachBehaviors($(data.selector));
};
