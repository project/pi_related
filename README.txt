Related Issues
==============

This module provides a simple mechanism for relating issues from Project issue
tracking module, to other issues, and provides backlinks for those relations.

This module also allows administrators to define custom relationships that can
be used for relating issues.

